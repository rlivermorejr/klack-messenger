const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema and Model

const userSchema = new Schema({
  name: String,
  active: Boolean,
  messages: Array,
});

const user = mongoose.model("user", userSchema);

module.exports = user;
