Today we will experiment with our own messaging app, klack (a portmanteau of Kenzie and Slack).

Start by checking out the code from: https://gitlab.com/kenzie-academy/se/fe/backend-persistent-storage/klack

screenshot-klack.png
Server

The Express server defined in app.js provides a messaging API supporting the following two endpoints:

GET /messages

Responds with a JSON object containing two fields: messages, an array of the last 40 messages, and users, an array of user objects. Each message object contains three fields: sender(a string), message(a string giving the text of the message), and timestamp. Each user object contains the fields name(a string) and active(a boolean).

POST /messages

Adds a new message to the front of the message list

Again, both endpoints deal with messages, which will be JSON objects with the following format:

{"sender": "Erika", "message": "It's snack time!"}

(messages can also include an optional timestamp field that is populated on the server, but not included in the initial POST by the client).
Client

The client app is in the public/ directory. It is written in HTML, CSS, and javascript.

The client uses the Fetch API to get/post messages to server.

It polls every 5 seconds for the latest messages and list of active users.

The server returns up to the 40 latest messages, but the client will skip over any messages with timestamps older than the last one shown.
Your tasks
Run the app and connect from several client machines

Install and run the klack server by following the instructions in the respository's README.md.

The README also gives you a reminder of how to find your machines IP address on the local network. Have several members of your group simultaneously point their browsers to a single server running on one of your machines, and try out the messaging functionality.
Look at the code

After seeing how the application behaves, look at how it is written.

Start by looking at the Express server defined in app.js. It's pretty short. Work with the other members of your group to see if you can understand what each line of code does.

Next, take a look at the client code in the public/ directory. The HTML and CSS should make sense to you. You may need to take a little more time to review the javascript. You don't need to scrutinize every line of the javascript, but at least examine the two fetch() calls closely and try to understand how they connect to the corresponding endpoints on the server.
Formulate questions about parts that don't make sense yet

Write down any questions you have about the code as you go. If there are parts of the code you can't figure out within your group, ask a coach, an instructor, or ask on the cohort Slack channel for help figuring it out.

You are encouraged to add console logging to the code and/or make use of the Chrome Developer Tools to study it in action. Make some guesses about what you think the state of various variables, arrays, and objects should be, then check to see if your expectations are correct.
Bonus: change it!

If you are feeling good about your understanding of the code, try modifying it further to add new features - or fix bugs! The current implementation is just a proof of concept, and has flaws. There is at least one bug in the client that will cause it to fail to display other user's messages if they are sent very soon before one of your own posts. Can you find the bug? (Please note that this is meant to be challenging bonus exercise -- don't feel bad if you can't find the bug!)
