const { assert } = require("assert");
const user = require("../models/data");

describe("saving to the database", function () {
  // Create tests
  it("saves a record to the database", function (done) {
    let char = new user({
      name: "User",
    });

    char.save().then(function () {
      assert(char.isNew === false);
      done();
    });
  });
});
